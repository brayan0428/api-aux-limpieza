<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');

require '../vendor/autoload.php';
require_once '../src/config/Conexion.php';
require_once '../src/config/Procesos.php';
require_once '../src/config/Consultas.php';
require_once '../lib/PayU.php';

$app = new \Slim\App;

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

require_once '../src/rutas/proveedores.php';
require_once '../src/rutas/maestros.php';
require_once '../src/rutas/auth.php';
require_once '../src/rutas/payu.php';
require_once '../src/rutas/dashboard/maestros.php';
require_once '../src/rutas/dashboard/upload.php';
require_once '../src/rutas/dashboard/auth.php';
require_once '../src/rutas/dashboard/proveedores.php';
require_once '../src/rutas/dashboard/agenda.php';
require_once '../src/rutas/dashboard/usuarios.php';
require_once '../src/rutas/dashboard/tarifas.php';
require_once '../src/rutas/dashboard/dashboard.php';
require_once '../src/rutas/dashboard/paquetes.php';

$app->run();