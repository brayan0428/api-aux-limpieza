<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();

$app->get('/admin/servicios', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->obtenerServiciosTodos());
}); 

$app->get('/admin/ciudades', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->ConsultarCiudades());
}); 

$app->get('/admin/grupos', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->ConsultarGrupoEspecialidades());
}); 

$app->get('/admin/especialidades/{grupo}', function(Request $request, Response $response) use($consultas) {
    $grupo = $request->getAttribute('grupo');
    return $response->withJson($consultas->ConsultarEspecialidadesxGrupo($grupo));
}); 

$app->post('/admin/servicio', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
        $servicio = $request->getParams();
        $procesos->beginTransaction();
        $msn = $procesos->guardarServicio($servicio);
        if ($msn !== "") {
            $procesos->rollbackTransaction();
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withJson(array("error"=>false,"message"=>"Servicio ingresado exitosamente"));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
}); 

$app->put('/admin/servicio', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
        $servicio = $request->getParams();
        $procesos->beginTransaction();
        $msn = $procesos->actualizarServicio($servicio);
        if ($msn !== "") {
            $procesos->rollbackTransaction();
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withJson(array("error"=>false,"message"=>"Servicio actualizado exitosamente"));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
}); 

$app->get('/admin/empleadas', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->obtenerEmpleadas());
});

$app->get('/admin/tipodoc', function(Request $request, Response $response) use($consultas) {
    return $consultas->obtenerTipoDocumento();
});

$app->post('/admin/empleada', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
        $empleada = $request->getParams();
        $codigo = $consultas->consultarConsecutivoEmpleada();
        if($codigo == 0){
            return $response->withStatus(500)->withJson(array("error"=>true,"message"=>"No se pudo establecer el consecutivo"));    
        }
        $procesos->beginTransaction();
        $msn = $procesos->guardarEmpleada($codigo,$empleada);
        if ($msn !== "") {
            $procesos->rollbackTransaction();
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withJson(array("error"=>false,"message"=>"Empleada ingresada exitosamente"));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
}); 

$app->put('/admin/empleada', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
        $empleada = $request->getParams();
        $procesos->beginTransaction();
        $msn = $procesos->actualizarEmpleada($empleada);
        if ($msn !== "") {
            $procesos->rollbackTransaction();
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withJson(array("error"=>false,"message"=>"Empleada actualizada exitosamente"));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
}); 

$app->get('/admin/proveedores/{id}', function(Request $request, Response $response) use($consultas) {
    $id = $request->getAttribute('id');
    return $response->withJson($consultas->ConsultarProveedores($id));
}); 

$app->post('/admin/ciudades', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
        $ciudad = $request->getParams();
        $procesos->beginTransaction();
        $msn = $procesos->GuardarCiudad($ciudad);
        if ($msn !== "") {
            $procesos->rollbackTransaction();
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withJson(array("error"=>false,"message"=>"Ciudad ingresada exitosamente"));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
}); 

$app->put('/admin/ciudades', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
        $ciudad = $request->getParams();
        $procesos->beginTransaction();
        $msn = $procesos->ActualizarCiudad($ciudad);
        if ($msn !== "") {
            $procesos->rollbackTransaction();
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withJson(array("error"=>false,"message"=>"Ciudad ingresada exitosamente"));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
}); 