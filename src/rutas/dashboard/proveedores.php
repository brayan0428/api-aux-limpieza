<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();

$app->post('/admin/proveedor', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
      $proveedor = $request->getParams();
      $existe = $consultas->consultarExisteProveedor($proveedor);
      if($existe){
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>"Ya se encuentra registrado en esa especialidad"));
      }
      $procesos->beginTransaction();
      $msn = $procesos->GuardarProveedor($proveedor);
      if (!is_numeric($msn)) {
        $procesos->rollbackTransaction();
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
      }
      $id_proveedor = $msn;
      extract($proveedor);
      if(!isset($especialidades)){
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>"Debe ingresar los servicios del proveedor"));
      }
      foreach($especialidades as $e){
        $msn = $procesos->insertarEspecialidadProveedor($id_proveedor,$e);
        if ($msn !== "") {
          $procesos->rollbackTransaction();
          return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
      }
      $procesos->commitTransaction();
      return $response->withJson(array("error"=>false,"message"=>""));
    } catch (Exception $e) {
      return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
});

$app->get('/admin/especialidades-proveedor/{id}', function(Request $request, Response $response) use($consultas) {
  $id = $request->getAttribute('id');
  return $response->withJson($consultas->ConsultarEspecialidadexProveedor($id));
});

$app->put('/admin/proveedor', function(Request $request, Response $response) use($consultas,$procesos) {
  try {    
    $proveedor = $request->getParams();
    $procesos->beginTransaction();
    $msn = $procesos->ActualizarProveedor($proveedor);
    if ($msn !== "") {
      $procesos->rollbackTransaction();
      return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
    }
    extract($proveedor);
    if(!isset($especialidades)){
      return $response->withStatus(400)->withJson(array("error"=>true,"message"=>"Debe ingresar los servicios del proveedor"));
    }
    $msn = $procesos->EliminarEspecialidadesProveedor($id);
    if ($msn !== "") {
      $procesos->rollbackTransaction();
      return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
    }
    foreach($especialidades as $e){
      $msn = $procesos->insertarEspecialidadProveedor($id,$e);
      if ($msn !== "") {
        $procesos->rollbackTransaction();
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
      }
    }
    $procesos->commitTransaction();
    return $response->withJson(array("error"=>false,"message"=>""));
  } catch (Exception $e) {
    return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
});