<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();

$app->get('/admin/tarifas', function(Request $request, Response $response) use($consultas) {
  return $response->withJson($consultas->ConsultarTarifas());
}); 

$app->post('/admin/tarifas', function(Request $request, Response $response) use($consultas) {
  $data = $request->getParams();
  return $response->withJson($consultas->ConsultarTarifa($data));
}); 

$app->post('/admin/tarifa', function(Request $request, Response $response) use($consultas,$procesos) {
  try {
    $data = $request->getParams();
    extract($data);
    $existe = $consultas->ConsultarExisteTarifa($tarifas[0]);
    if($existe){
      return $response->withStatus(400)->withJson(array("error"=>true,"message"=>"Ya existe una tarifa registrada para ese servicio, año y ciudad"));
    }
    $procesos->beginTransaction();
    foreach ($tarifas as $tarifa) {
      $msn = $procesos->Ingresar_Tarifa($tarifa);
      if($msn != ""){
        $procesos->rollBackTransaction();
        return $response->withJson(array("error"=>true,"message"=>$msn));
      }
    }
    $procesos->commitTransaction();
    return $response->withStatus(200)->withJson(array("error"=>false,"message"=>"Tarifa agregada exitosamente"));
  } catch (Exception $e) {
    return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
}); 

$app->put('/admin/tarifa', function(Request $request, Response $response) use($consultas,$procesos) {
  try {
    $data = $request->getParams();
    extract($data);
    $procesos->beginTransaction();
    foreach ($tarifas as $tarifa) {
      $msn = $procesos->Actualizar_Tarifa($tarifa);
      if($msn != ""){
        $procesos->rollBackTransaction();
        return $response->withJson(array("error"=>true,"message"=>$msn));
      }
    }
    $procesos->commitTransaction();
    return $response->withStatus(200)->withJson(array("error"=>false,"message"=>"Tarifa actualizada exitosamente"));
  } catch (Exception $e) {
    return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
}); 