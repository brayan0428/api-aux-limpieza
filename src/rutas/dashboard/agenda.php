<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();


$app->get('/admin/clientes', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->ConsultarClientes());
}); 

$app->get('/admin/clientes-asociados/{id}', function(Request $request, Response $response) use($consultas) {
    $id = $request->getAttribute('id');
    return $response->withJson($consultas->ConsultarClientesAsociados($id));
}); 

$app->get('/admin/formaspago', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->ConsultarFormasPago());
}); 

$app->post('/admin/servicios', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
        $data = $request->getParams();
        extract($data);
        $consecutivo = $consultas->consultarConsecutivoServicio();
        $valor_unitario = $total / $dias;
        $data["valor"] = $valor_unitario;
        $procesos->beginTransaction();
        for($i=1;$i<=$dias;$i++){
            $msn = $procesos->NuevoServicio($consecutivo,$fecha_inicio,$data);
            if($msn != ""){
              $procesos->rollBackTransaction();
              return $response->withJson(array("error"=>true,"message"=>$msn));
            }
            $fecha_inicio = date('Y-m-d', strtotime($fecha_inicio . " + 1 day"));
        }
        $procesos->commitTransaction();
        return $response->withStatus(200)->withJson(array("error"=>false,"message"=>"Servicio agregado exitosamente"));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
});

$app->get('/admin/agenda', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->ConsultarAgenda());
}); 

$app->get('/admin/servicio/{id}', function(Request $request, Response $response) use($consultas) {
    $id = $request->getAttribute('id');
    return $response->withJson($consultas->ConsultarInfoServicio($id));
});

$app->put('/admin/servicio/{id}', function(Request $request, Response $response) use($consultas, $procesos) {
    try {
        $id = $request->getAttribute('id');
        $data = $request->getParams();
        $procesos->beginTransaction();
        $msn = $procesos->Actualizar_Servicio($id,$data);
        if($msn != ""){
            $procesos->rollBackTransaction();
            return $response->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withStatus(200)->withJson(array("error"=>false,"message"=>"Servicio actualizado exitosamente"));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
});

$app->delete('/admin/servicio/{id}', function(Request $request, Response $response) use($consultas, $procesos) {
    try {
        $id = $request->getAttribute('id');
        $procesos->beginTransaction();
        $msn = $procesos->Eliminar_Servicio($id);
        if($msn != ""){
            $procesos->rollBackTransaction();
            return $response->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withStatus(200)->withJson(array("error"=>false,"message"=>"Servicio eliminado exitosamente"));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
});