<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$consultas = new Consultas();
$procesos = new Procesos();

$app->get('/admin/paquetes', function(Request $request, Response $response) use($consultas) {
  return $response->withJson($consultas->ConsultarPaquetes());
}); 

$app->get('/admin/ciudades-paquete/{id}', function(Request $request, Response $response) use($consultas) {
    $id = $request->getAttribute('id');
    return $response->withJson($consultas->ConsultarCiudadesPaquete($id));
}); 

$app->post('/admin/paquete', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
      $paquete = $request->getParams();
      $procesos->beginTransaction();
      $msn = $procesos->GuardarPaquete($paquete);
      if (!is_numeric($msn)) {
        $procesos->rollbackTransaction();
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
      }
      $id_paquete = $msn;
      extract($paquete);
      if(!isset($ciudades)){
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>"Debe seleccionar las ciudades del paquete"));
      }
      foreach($ciudades as $c){
        $msn = $procesos->GuardarCiudadPaquete($id_paquete,$c);
        if ($msn !== "") {
          $procesos->rollbackTransaction();
          return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
      }
      $procesos->commitTransaction();
      return $response->withJson(array("error"=>false,"message"=>""));
    } catch (Exception $e) {
      return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
});

$app->put('/admin/paquete/{id}', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
      $id = $request->getAttribute('id');
      $paquete = $request->getParams();
      $procesos->beginTransaction();
      $msn = $procesos->ActualizarPaquete($id,$paquete);
      if ($msn !== "") {
        $procesos->rollbackTransaction();
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
      }
      extract($paquete);
      if(!isset($ciudades)){
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>"Debe seleccionar las ciudades del paquete"));
      }
      $msn = $procesos->EliminarCiudadesPaquete($id);
      if ($msn !== "") {
        $procesos->rollbackTransaction();
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
      }
      foreach($ciudades as $c){
        $msn = $procesos->GuardarCiudadPaquete($id,$c);
        if ($msn !== "") {
          $procesos->rollbackTransaction();
          return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
      }
      $procesos->commitTransaction();
      return $response->withJson(array("error"=>false,"message"=>""));
    } catch (Exception $e) {
      return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
});