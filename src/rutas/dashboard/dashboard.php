<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();

$app->get('/admin/charts/{tipo}', function(Request $request, Response $response) use($consultas) {
  $tipo = $request->getAttribute('tipo');
  return $response->withJson($consultas->ConsultarDatosGraficas($tipo));
});

$app->get('/admin/reportes/{tipo}', function(Request $request, Response $response) use($consultas) {
  $tipo = $request->getAttribute('tipo');
  return $response->withJson($consultas->ConsultarReportes($tipo));
});