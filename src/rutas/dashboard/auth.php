<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();

$app->post('/auth/login', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
        $user = $request->getParams();
        $data = $consultas->consultarLoginDashboard($user);
        if(count($data) == 0){
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>"Usuario o Clave Invalidos"));
        }
        return $response->withJson($data);
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
}); 