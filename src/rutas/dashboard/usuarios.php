<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();

$app->get('/admin/usuarios', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->ConsultarUsuarios());
}); 

$app->post('/admin/usuarios', function(Request $request, Response $response) use($procesos,$consultas) {
    try {    
        $user = $request->getParams();
        extract($user);
        $existe = $consultas->consultarExisteUsuario($correo);
        if($existe){
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>"Ya existe un usuario registrado con ese correo"));
        }
        $existe = $consultas->consultarExisteUsuarioxCedula($cedula);
        if($existe){
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>"Ya existe un usuario registrado con ese numero de cedula"));
        }
        $procesos->beginTransaction();
        $msn = $procesos->insertarNuevoUsuario($user);
        if (!is_numeric($msn)) {
            $procesos->rollbackTransaction();
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
        $id_usuario = $msn;
        $user["id_usuario"] = $id_usuario;
        $user["nombre"] = $nombres . " ". $apellidos;
        $msn = $procesos->insertarUsuarioAsociado($user,1);
        if ($msn !== "") {
            $procesos->rollbackTransaction();
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withJson(array("error"=>false,"message"=>""));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
}); 

$app->put('/admin/usuarios', function(Request $request, Response $response) use($procesos,$consultas) {
    try {    
        $user = $request->getParams();
        extract($user);
        $procesos->beginTransaction();
        $msn = $procesos->Actualizar_Usuario($user);
        if ($msn !== "") {
            $procesos->rollbackTransaction();
            return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withJson(array("error"=>false,"message"=>""));
    } catch (Exception $e) {
        return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
});

$app->post('/admin/cliente-asociado', function(Request $request, Response $response) use($consultas,$procesos) {
    try {    
      $user = $request->getParams();
      extract($user);
      $procesos->beginTransaction();
      $msn = "";
      if($id == 0){
        $msn = $procesos->insertarUsuarioAsociado($user,0);
      }else{
        $msn = $procesos->actualizarUsuarioAsociado($user);
      }
      if ($msn !== "") {
        $procesos->rollbackTransaction();
        return $response->withStatus(400)->withJson(array("error"=>true,"message"=>$msn));
      }
      $procesos->commitTransaction();
      return $response->withJson(array("error"=>false,"message"=>"Información guardada exitosamente"));
    } catch (Exception $e) {
      return $response->withStatus(500)->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
});