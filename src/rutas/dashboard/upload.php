<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

function moveUploadedFile($directory, $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename =  pathinfo($uploadedFile->getClientFilename(), PATHINFO_FILENAME);
    $filename = sprintf('%s.%0.8s', $basename . "_" . date("His"), $extension);
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}

$app->post("/admin/upload", function(Request $request, Response $response){
    try {
    $directory = $_SERVER["DOCUMENT_ROOT"] . "/images";
    //$directory = $_SERVER["DOCUMENT_ROOT"] . "/api-aux-limpieza/images";
    extract($request->getParams());
    $directory = $directory . "/" . $type;
    if (!file_exists($directory)) {
        mkdir($directory, 0777, true);
    }
    $uploadedFiles = $request->getUploadedFiles();
    $uploadedFile = $uploadedFiles["file"];
    if($uploadedFile->getError() === UPLOAD_ERR_OK){
        $filename = moveUploadedFile($directory, $uploadedFile);
        return $response->withJson(array("error"=>false,"message"=> $filename));
    }
    } catch (Exception $e) {
        return $response->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
});