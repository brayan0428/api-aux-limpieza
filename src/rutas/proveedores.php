<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();


$app->post('/proveedor', function(Request $request, Response $response) use($consultas,$procesos) {
  try {    
    $proveedor = $request->getParams();
    /*$existe = $consultas->consultarExisteProveedor($proveedor);
    if($existe){
      return $response->withJson(array("error"=>true,"message"=>"Ya se encuentra registrado en esa especialidad"));
    }*/
    $procesos->beginTransaction();
    $msn = $procesos->insertarNuevoProveedor($proveedor);
    if (!is_numeric($msn)) {
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$msn));
    }
    $id_proveedor = $msn;
    extract($proveedor);
    foreach($especialidades as $e){
      $msn = $procesos->insertarEspecialidadProveedor($id_proveedor,$e);
      if ($msn !== "") {
        $procesos->rollbackTransaction();
        return $response->withJson(array("error"=>true,"message"=>$msn));
      }
    }
    $procesos->commitTransaction();
    return $response->withJson(array("error"=>false,"message"=>""));
  } catch (Exception $e) {
    return $response->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
  
}); 

$app->get('/proveedores/{especialidad}', function(Request $request, Response $response) use($consultas) {
  $especialidad = $request->getAttribute('especialidad');
  return $response->withJson($consultas->obtenerProveedores($especialidad));
}); 

$app->post('/pre-proveedor', function(Request $request, Response $response) use($consultas,$procesos) {
  try {    
    $proveedor = $request->getParams();
    $existe = $consultas->consultarExisteProveedorxEmail($proveedor);
    if($existe){
      return $response->withJson(array("error"=>true,"message"=>"El email ya se encuentra registrado"));
    }
    $procesos->beginTransaction();
    $msn = $procesos->insertarPreProveedor($proveedor);
    if (!is_numeric($msn)) {
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$msn));
    }
    $id_proveedor = $msn;
    extract($proveedor);
    foreach($especialidades as $e){
      $msn = $procesos->insertarEspecialidadProveedor($id_proveedor,$e);
      if ($msn !== "") {
        $procesos->rollbackTransaction();
        return $response->withJson(array("error"=>true,"message"=>$msn));
      }
    }
    $procesos->commitTransaction();
    return $response->withJson(array("error"=>false,"message"=>$msn));
  } catch (Exception $e) {
    return $response->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
}); 