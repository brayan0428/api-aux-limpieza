<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();

$app->get('/ciudades', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->obtenerCiudades());
}); 

$app->get('/grupos', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->obtenerGrupos());
}); 

$app->get('/tipodoc', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->obtenerTipoDocumento());
}); 

$app->get('/servicios-especiales-todos/{grupo}', function(Request $request, Response $response) use($consultas) {
    $grupo = $request->getAttribute('grupo');
    return $response->withJson($consultas->obtenerServiciosEspecialesTodos($grupo));
}); 

$app->get('/servicios-especiales/{grupo}', function(Request $request, Response $response) use($consultas) {
    $grupo = $request->getAttribute('grupo');
    return $response->withJson($consultas->obtenerServiciosEspeciales($grupo));
}); 

$app->get('/servicios', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->obtenerServicios());
}); 

$app->get('/usuarios-asociados/{id}', function(Request $request, Response $response) use($consultas) {
    $id = $request->getAttribute('id');
    return $response->withJson($consultas->obtenerUsuariosAsociados($id));
}); 

$app->post('/valorservicio', function(Request $request, Response $response) use($consultas) {
    $data = $request->getParams();
    return $response->withJson($consultas->consultarValorServicio($data));
}); 

$app->get('/festivos', function(Request $request, Response $response) use($consultas) {
    return $response->withJson($consultas->obtenerDiasFestivos());
}); 

$app->post('/usuario-asociado', function(Request $request, Response $response) use($consultas,$procesos) {
  try {    
    $user = $request->getParams();
    extract($user);
    $procesos->beginTransaction();
    $msn = "";
    if($id == 0){
      $msn = $procesos->insertarUsuarioAsociado($user,0);
    }else{
      $msn = $procesos->actualizarUsuarioAsociado($user);
    }
    if ($msn !== "") {
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$msn));
    }
    $procesos->commitTransaction();
    return $response->withJson(array("error"=>false,"message"=>"Usuario ingresado exitosamente"));
  } catch (Exception $e) {
    return $response->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
}); 

$app->post('/eliminar-usuario-asociado', function(Request $request, Response $response) use($consultas,$procesos) {
  try {    
    $user = $request->getParams();
    extract($user);
    $procesos->beginTransaction();
    $msn = $procesos->eliminarUsuarioAsociado($user);
    if ($msn !== "") {
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$msn));
    }
    $procesos->commitTransaction();
    return $response->withJson(array("error"=>false,"message"=>"Usuario eliminado exitosamente"));
  } catch (Exception $e) {
    return $response->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
}); 

$app->get('/serviciosxusuario/{id}', function(Request $request, Response $response) use($consultas) {
  $id = $request->getAttribute('id');
  return $response->withJson($consultas->ConsultarServiciosxUsuario($id));
}); 

$app->get('/usuario/{id}', function(Request $request, Response $response) use($consultas) {
  $id = $request->getAttribute('id');
  return $response->withJson($consultas->ConsultarUsuario($id));
}); 