<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$consultas = new Consultas();
$procesos = new Procesos();

/*PayU::$apiKey = "n3w5bVC1k2361JS0LCXn9zJ0ki"; //apiKey Real.
PayU::$apiLogin = "F762Zgq0aD0y9pv"; //apiLogin Real.
PayU::$merchantId = "677180"; //Id de Comercio Real.*/

PayU::$apiKey = "4Vj8eK4rloUd272L48hsrarnUA"; //apiKey de prueba.
PayU::$apiLogin = "pRRXKOl8ikMmt9u"; //apiLogin de prueba.
PayU::$merchantId = "508029"; //Id de Comercio de prueba.

PayU::$language = SupportedLanguages::ES; //Seleccione el idioma.
PayU::$isTest = true; //Dejarlo True cuando sean pruebas.

Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
//Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi");

$parameters = array(
    //Ingrese aquí el número de cuotas.
    PayUParameters::INSTALLMENTS_NUMBER => "1",
    //Ingrese aquí el nombre del país.
    PayUParameters::COUNTRY => PayUCountries::CO,
    //Ingrese aquí el identificador de la cuenta.
    //ayUParameters::ACCOUNT_ID => "679925",
    PayUParameters::ACCOUNT_ID => "512321",
    //Cookie de la sesión actual.
    PayUParameters::PAYER_COOKIE => "cookie_" . time(),
    //Ingrese aquí la moneda.
    PayUParameters::CURRENCY => "COP",
    //Se ingresa el id de usuario, una referencia del sistema
    PayUParameters::PAYER_ID => "125",
);

$app->post('/credit-card', function(Request $request, Response $response) use($parameters,$consultas,$procesos) {
    $data = $request->getParams();
    extract($data);
    $consecutivo = $consultas->consultarConsecutivoServicio();
    $empleada = $consultas->consultarEmpleadaNuevoServicio($ciudad);
    if($empleada == 0){
      return $response->withJson(array("error"=>true,"message"=>"Error al asignar empleada"));
    }
    $data["id_empleado"] = $empleada;
    $valorTotal = $data["valor"];
    $procesos->beginTransaction();
    for($i=1;$i<=$cantidad_dias;$i++){
      $data["valor"] =  $data["valor_normal"];
      if(date("N", strtotime($fecha_inicio)) == 7 || $consultas->existeDiaFestivo($fecha_inicio)){
        $data["valor"] = $data["valor_especial"];
      }
      $data["estado"] = 2;
      if($i > 1){
        $fecha_inicio = "1900-01-01";
        $data["estado"] = 1;
        $data["id_empleado"] = 0;
      }
      $msn = $procesos->insertarServicio($consecutivo,$fecha_inicio,$data);
      if($msn != ""){
        $procesos->rollBackTransaction();
        return $response->withJson(array("error"=>true,"message"=>$msn));
      }
      $fecha_inicio = date('Y-m-d', strtotime($fecha_inicio . " + 1 day"));
    }
    try {    
        $parameters[PayUParameters::PAYER_NAME] = $nombre_titular;
        $parameters[PayUParameters::CREDIT_CARD_NUMBER] = $numero_tarjeta;
        $parameters[PayUParameters::CREDIT_CARD_EXPIRATION_DATE] = $expiracion;
        $parameters[PayUParameters::CREDIT_CARD_SECURITY_CODE] = $cvv;
        $parameters[PayUParameters::PROCESS_WITHOUT_CVV2] = false;
        $parameters[PayUParameters::PAYMENT_METHOD] = $tipo_tarjeta;
        //Ingrese aquí el código de referencia.
        $parameters[PayUParameters::REFERENCE_CODE] = $data["nombreServicio"] . "-" . $consecutivo . "-" . $data["id_usuario"];
        //Ingrese aquí la descripción.
        $parameters[PayUParameters::DESCRIPTION] = "Servicio de " .  $data["nombreServicio"];
        //Ingrese aquí el valor o monto a pagar.
        $parameters[PayUParameters::VALUE] = $valorTotal;
        //Ingrese aquí su firma. “{APIKEY}~{MERCHANTID}~{REFERENCE_CODE}~{VALUE}~{CURRENCY}”
        $parameters[PayUParameters::SIGNATURE] = md5(PayU::$apiKey . "~" . PayU::$merchantId . "~" .$parameters[PayUParameters::REFERENCE_CODE] . "~" .  $valorTotal . "~COP");
        $payu_response = PayUPayments::doAuthorizationAndCapture($parameters);
		if ($payu_response->code == "SUCCESS") {
			if ($payu_response->transactionResponse->state == "APPROVED") {
        $msn = $procesos->insertarAbono($consecutivo,$data["id_usuario"],$valorTotal,$payu_response->transactionResponse->transactionId,"TC");
        if($msn != ""){
          $procesos->rollBackTransaction();
          return $response->withJson(array("error"=>true,"message"=>$msn));
        }
        $procesos->commitTransaction();
        return $response->withJson(array("error"=>false,"message"=>"Ok"));
			}else{
        $procesos->rollBackTransaction();
        return $response->withJson(array("error"=>true,"message"=>$payu_response->transactionResponse->responseCode));
      }
		} else {
      $procesos->rollBackTransaction();
			return $response->withJson(array("error"=>true,"message"=>""));
		}
    } catch (Exception $e) {
      return $response->withJson(array("error"=>true,"message"=>$e->getMessage()));
    }
  }); 