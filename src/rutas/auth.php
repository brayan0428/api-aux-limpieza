<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use PHPMailer\PHPMailer\PHPMailer;
//$app = new \Slim\App;

$consultas = new Consultas();
$procesos = new Procesos();

function EnviarEmail($email, $asunto, $plantilla) {
  try {  
    $body = $plantilla;
    $mail = new PHPMailer;
    $mail->SMTPSecure = 'ssl';
    $mail->Host = "mail.auxlimpieza.com";
    $mail->Port = 465;
    $mail->IsSMTP(); // use SMTP
    $mail->SMTPAuth = true;
    $mail->Username = 'notificaciones@auxlimpieza.com';
    $mail->Password = '1DXcO#Rhv!bO4g2';
    $mail->setFrom('notificaciones@auxlimpieza.com');
    $mail->addAddress($email);
    $mail->Subject = $asunto;
    $mail->isHtml(true);
    $mail->Body = $body;
    if (!$mail->Send()) {
        return 'Error al enviar email: ' . $mail->ErrorInfo;
    } 
    return '';
  } catch (Exception $e) {
    return $e->getMessage();
  }
}

function GenerarClaveAleatoria(){
  $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
  return substr(str_shuffle($permitted_chars), 0, 5);
}

$app->post('/usuario', function(Request $request, Response $response) use($consultas) {
    $user = $request->getParams();
    return $consultas->consultarLogin($user);
}); 

$app->post('/usuarionuevo', function(Request $request, Response $response) use($procesos,$consultas) {
  try {    
    $user = $request->getParams();
    extract($user);
    $existe = $consultas->consultarExisteUsuario($correo);
    if($existe){
      return $response->withJson(array("error"=>true,"message"=>"Ya existe un usuario registrado con ese correo"));
    }
    $procesos->beginTransaction();
    $msn = $procesos->insertarNuevoUsuario($user);
    if (!is_numeric($msn)) {
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$msn));
    }
    $id_usuario = $msn;
    $user["id_usuario"] = $id_usuario;
    $user["nombre"] = $nombres . " ". $apellidos;
    $msn = $procesos->insertarUsuarioAsociado($user,1);
    if ($msn !== "") {
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$msn));
    }
    $plantilla = file_get_contents('emails/registro.html',FILE_USE_INCLUDE_PATH);
    $email = EnviarEmail($correo, "AUX Limpieza - Registro Exitoso", $plantilla);
    if($email != ''){
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$email));
    }
    $procesos->commitTransaction();
    return $response->withJson(array("error"=>false,"message"=>""));
  } catch (Exception $e) {
    return $response->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
  
}); 

$app->post('/actualizar-usuario', function(Request $request, Response $response) use($procesos,$consultas) {
  try {   
    $user = $request->getParams();
    extract($user);
    $procesos->beginTransaction();
    $msn = $procesos->actualizarUsuario($user);
    if ($msn !== "") {
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$msn));
    }
    $user["nombre"] = $nombres . " ". $apellidos;
    $msn = $procesos->actualizarUsuarioPrincipal($user);
    if ($msn !== "") {
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$msn));
    }
    $procesos->commitTransaction();
    return $response->withJson(array("error"=>false,"message"=>""));
  } catch (Exception $e) {
    return $response->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
  
}); 

$app->post('/olvido-contrasena', function(Request $request, Response $response) use($procesos,$consultas) {
  try {   
    $data = $request->getParams();
    extract($data);
    $existe = $consultas->consultarExisteUsuarioxEmail($data);
    if(!$existe){
      return $response->withJson(array("error"=>true,"message"=>"No existe un usuario registrado con ese correo"));
    }
    $claveAleatoria = GenerarClaveAleatoria();
    $procesos->beginTransaction();
    $msn = $procesos->ActualizarClave($data, $claveAleatoria);
    if ($msn !== "") {
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$msn));
    }
    $plantilla = file_get_contents('emails/olvido_contrasena.html',FILE_USE_INCLUDE_PATH);
    $plantilla = str_replace("{claveAleatoria}", $claveAleatoria, $plantilla);
    /*$email = EnviarEmail($email, "AUX Limpieza - Olvido Contraseña", $plantilla);
    if($email != ''){
      $procesos->rollbackTransaction();
      return $response->withJson(array("error"=>true,"message"=>$email));
    }*/
    $procesos->commitTransaction();
    return $response->withJson(array("error"=>false,"message"=>""));
  } catch (Exception $e) {
    return $response->withJson(array("error"=>true,"message"=>$e->getMessage()));
  }
});
