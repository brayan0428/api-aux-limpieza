<?php

class Procesos{
  private $db;

  public function __construct(){
     $conn = new Conexion();
     $this->db = $conn->conexion();
  }

  public function beginTransaction(){
    $this->db->beginTransaction();
  }

  public function rollBackTransaction(){
    $this->db->rollBack();
  }

  public function commitTransaction(){
    $this->db->commit();
  }

  public function insertarNuevoProveedor($proveedor){
    
    $sql = "insert into proveedores(tipodoc,cedula,nombres,apellidos,direccion,ciudad,email,celular,sitio_web,descripcion,nombre_referencia_personal,telefono_referencia_personal,nombre_referencia_familiar,telefono_referencia_familiar,habilitado)
    values(:tipo_doc,:cedula,:nombres,:apellidos,:direccion,:ciudad,:email,:celular,:sitio_web,:descripcion,:nombre_referencia_personal,:telefono_referencia_personal,:nombre_referencia_familiar,:telefono_referencia_familiar,0)";

    try {
      extract($proveedor);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':tipo_doc',$tipo_doc);
      $stm->bindParam(':cedula',$cedula);
      $stm->bindParam(':nombres',$nombres);
      $stm->bindParam(':apellidos',$apellidos);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':email',$email);
      $stm->bindParam(':celular',$celular);
      $stm->bindParam(':sitio_web',$sitio_web);
      $stm->bindParam(':descripcion',$descripcion);
      $stm->bindParam(':nombre_referencia_personal',$nombre_referencia_personal);
      $stm->bindParam(':telefono_referencia_personal',$telefono_referencia_personal);
      $stm->bindParam(':nombre_referencia_familiar',$nombre_referencia_familiar);
      $stm->bindParam(':telefono_referencia_familiar',$telefono_referencia_familiar);
      $stm->execute();
      return $this->db->lastInsertId();
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function insertarNuevoUsuario($user){
    
    $sql = "insert into usuarios (cedula,nombres,apellidos,correo,direccion,ciudad,telefono,id_perfil,clave,habilitado,fechaing)
    values(:cedula,:nombres,:apellidos,:correo,:direccion,:ciudad,:telefono,:perfil,:clave,1,NOW())";
    try {
      extract($user);
      $clave = md5($clave);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':cedula',$cedula);
      $stm->bindParam(':nombres',$nombres);
      $stm->bindParam(':apellidos',$apellidos);
      $stm->bindParam(':correo',$correo);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':telefono',$telefono);
      $stm->bindParam(':perfil',$perfil);
      $stm->bindParam(':clave',$clave);
      $stm->execute();
      return $this->db->lastInsertId();;
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function insertarPreProveedor($proveedor){
    
    $sql = "INSERT INTO proveedores (nombres,apellidos,ciudad,celular,email,descripcion, habilitado)
            VALUES (:nombres,:apellidos,:ciudad,:celular,:email,:descripcion, 0)";
    try {
      extract($proveedor);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':nombres',$nombres);
      $stm->bindParam(':apellidos',$apellidos);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':email',$email);
      $stm->bindParam(':celular',$celular);
      $stm->bindParam(':descripcion',$descripcion);
      $stm->execute();
      return $this->db->lastInsertId();
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function insertarEspecialidadProveedor($id_proveedor,$id_especialidad){
    
    $sql = "INSERT INTO especialidades_proveedores (id_proveedor,id_especialidad)
            VALUES (:id_proveedor,:id_especialidad)";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id_proveedor',$id_proveedor);
      $stm->bindParam(':id_especialidad',$id_especialidad);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function insertarServicio($consecutivo,$fecha,$data){
    extract($data);
    $sql = "INSERT INTO servicios(consecutivo,id_usuario,id_empleada,servicio,fecha_servicio,direccion,ciudad,horas,hora_inicio,hora_fin,valor,estado, anulado)
    VALUES(:consecutivo,:id_usuario,:id_empleada,:servicio,:fecha_servicio,:direccion,:ciudad,:horas,:hora_inicio,:hora_fin,:valor,:estado, 0)";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':consecutivo',$consecutivo);
      $stm->bindParam(':id_usuario',$id_usuario);
      $stm->bindParam(':id_empleada',$id_empleado);
      $stm->bindParam(':servicio',$servicio);
      $stm->bindParam(':fecha_servicio',$fecha);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':horas',$horas);
      $stm->bindParam(':hora_inicio',$hora_inicio);
      $stm->bindParam(':hora_fin',$hora_fin);
      $stm->bindParam(':valor',$valor);
      $stm->bindParam(':estado',$estado);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function insertarAbono($consecutivo,$id_usuario,$valor,$id_transaccion,$forma_pago){
    
    $sql = "INSERT INTO abonos (consecutivo,fecha,id_usuario,valor,id_transaccion,forma_pago)
    VALUES(:consecutivo,NOW(),:id_usuario,:valor,:id_transaccion,:forma_pago)";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':consecutivo',$consecutivo);
      $stm->bindParam(':valor',$valor);
      $stm->bindParam(':id_usuario',$id_usuario);
      $stm->bindParam(':forma_pago',$forma_pago);
      $stm->bindParam(':id_transaccion',$id_transaccion);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function insertarUsuarioAsociado($user,$usuario_principal){
    extract($user);
    $sql = "insert into usuarios_asociados(id_usuario,nombre,direccion,ciudad,telefono,habilitado,usuario_principal)
            values(:id_usuario,:nombre,:direccion,:ciudad,:telefono,1,:usuario_principal)";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id_usuario',$id_usuario);
      $stm->bindParam(':nombre',$nombre);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':telefono',$telefono);
      $stm->bindParam(':usuario_principal',intval($usuario_principal));
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function actualizarUsuarioAsociado($user){
    extract($user);
    $sql = "update usuarios_asociados set nombre=:nombre,direccion=:direccion,ciudad=:ciudad,telefono=:telefono
            where id_usuario=:id_usuario and id=:id";            
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id_usuario',$id_usuario);
      $stm->bindParam(':nombre',$nombre);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':telefono',$telefono);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function actualizarUsuarioPrincipal($user){
    extract($user);
    $sql = "update usuarios_asociados set nombre=:nombre,direccion=:direccion,ciudad=:ciudad,telefono=:telefono
            where id_usuario=:id_usuario and usuario_principal = 1";            
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id_usuario',$id_usuario);
      $stm->bindParam(':nombre',$nombre);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':telefono',$telefono);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

   public function eliminarUsuarioAsociado($user){
    extract($user);
    $sql = "update usuarios_asociados SET habilitado = 0 WHERE id_usuario = :id_usuario AND id = :id";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id_usuario',$id_usuario);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function actualizarUsuario($user){
    extract($user);
    $sql = "update usuarios SET nombres= :nombres, apellidos = :apellidos, correo = :correo, direccion = :direccion, ciudad = :ciudad, telefono = :telefono WHERE id = :id";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':nombres',$nombres);
      $stm->bindParam(':apellidos',$apellidos);
      $stm->bindParam(':correo',$correo);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':telefono',$telefono);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function guardarServicio($servicio){
    extract($servicio);
    $sql = "INSERT INTO mtservicios (codigo, descripcion,icon,habilitado) VALUES (:codigo,:descripcion,:icon,:habilitado)";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':codigo',$codigo);
      $stm->bindParam(':descripcion',$descripcion);
      $stm->bindParam(':icon',$icon);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function actualizarServicio($servicio){
    extract($servicio);
    $sql = "UPDATE mtservicios SET descripcion = :descripcion, icon = :icon, habilitado = :habilitado WHERE codigo = :codigo";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':codigo',$codigo);
      $stm->bindParam(':descripcion',$descripcion);
      $stm->bindParam(':icon',$icon);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function guardarEmpleada($codigo,$empleada){
    extract($empleada);
    $sql = "INSERT INTO empleadas(codigo,cedula, nombre,ciudad,direccion,celular,email,habilitado, imagen)
    VALUES(:codigo,:cedula,:nombre,:ciudad,:direccion,:celular,:email,:habilitado, :imagen)";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':codigo',$codigo);
      $stm->bindParam(':cedula',$cedula);
      $stm->bindParam(':nombre',$nombre);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':celular',$celular);
      $stm->bindParam(':email',$email);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->bindParam(':imagen',$imagen);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function actualizarEmpleada($empleada){
    extract($empleada);
    $sql = "UPDATE empleadas SET cedula=:cedula,nombre=:nombre,ciudad=:ciudad,direccion=:direccion,celular=:celular,email=:email,
    habilitado=:habilitado, imagen = :imagen WHERE codigo = :codigo";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':codigo',$codigo);
      $stm->bindParam(':cedula',$cedula);
      $stm->bindParam(':nombre',$nombre);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':celular',$celular);
      $stm->bindParam(':email',$email);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->bindParam(':imagen',$imagen);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function GuardarProveedor($proveedor){
    
    $sql = "INSERT INTO proveedores(tipodoc,cedula,nombres,apellidos,direccion,ciudad,celular, email,sitio_web, descripcion, nombre_referencia_personal, telefono_referencia_personal,nombre_referencia_familiar, telefono_referencia_familiar,habilitado)
    VALUES (:tipodoc,:cedula,:nombres,:apellidos,:direccion,:ciudad,:celular,:email,:sitio_web, :descripcion, :nombre_referencia_personal, :telefono_referencia_personal,:nombre_referencia_familiar, :telefono_referencia_familiar,:habilitado)";

    try {
      extract($proveedor);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':tipodoc',$tipodoc);
      $stm->bindParam(':cedula',$cedula);
      $stm->bindParam(':nombres',$nombres);
      $stm->bindParam(':apellidos',$apellidos);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':email',$email);
      $stm->bindParam(':celular',$celular);
      $stm->bindParam(':sitio_web',$sitio_web);
      $stm->bindParam(':descripcion',$descripcion);
      $stm->bindParam(':nombre_referencia_personal',$nombre_referencia_personal);
      $stm->bindParam(':telefono_referencia_personal',$telefono_referencia_personal);
      $stm->bindParam(':nombre_referencia_familiar',$nombre_referencia_familiar);
      $stm->bindParam(':telefono_referencia_familiar',$telefono_referencia_familiar);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->execute();
      return $this->db->lastInsertId();
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function ActualizarProveedor($proveedor){
    
    $sql = "UPDATE proveedores SET tipodoc = :tipodoc,cedula=:cedula,nombres=:nombres,apellidos=:apellidos,direccion=:direccion, ciudad = :ciudad,celular=:celular,email=:email,sitio_web = :sitio_web, descripcion = :descripcion, 
    nombre_referencia_personal = :nombre_referencia_personal, telefono_referencia_personal = :telefono_referencia_personal,telefono_referencia_familiar = :telefono_referencia_familiar,
    nombre_referencia_familiar = :nombre_referencia_familiar, habilitado = :habilitado WHERE id = :id";

    try {
      extract($proveedor);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':tipodoc',$tipodoc);
      $stm->bindParam(':cedula',$cedula);
      $stm->bindParam(':nombres',$nombres);
      $stm->bindParam(':apellidos',$apellidos);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':email',$email);
      $stm->bindParam(':celular',$celular);
      $stm->bindParam(':sitio_web',$sitio_web);
      $stm->bindParam(':descripcion',$descripcion);
      $stm->bindParam(':nombre_referencia_personal',$nombre_referencia_personal);
      $stm->bindParam(':telefono_referencia_personal',$telefono_referencia_personal);
      $stm->bindParam(':nombre_referencia_familiar',$nombre_referencia_familiar);
      $stm->bindParam(':telefono_referencia_familiar',$telefono_referencia_familiar);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function EliminarEspecialidadesProveedor($id){
    $sql = "DELETE FROM especialidades_proveedores WHERE id_proveedor = :id";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function GuardarCiudad($ciudad){
    extract($ciudad);
    $sql = "INSERT INTO mtciudades(codigo, nombre,habilitado) VALUES (:codigo, :nombre,:habilitado)";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':codigo',$codigo);
      $stm->bindParam(':nombre',$nombre);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function ActualizarCiudad($ciudad){
    extract($ciudad);
    $sql = "UPDATE mtciudades SET nombre = :nombre,habilitado = :habilitado WHERE id = :id";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id',$id);
      $stm->bindParam(':nombre',$nombre);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function NuevoServicio($consecutivo,$fecha,$data){
    extract($data);
    $sql = "INSERT INTO servicios(consecutivo,id_usuario,id_empleada,servicio,fecha_servicio,direccion,ciudad,horas,hora_inicio,hora_fin,valor,estado,formapago, anulado, fecha_ing)
    VALUES(:consecutivo,:id_usuario,:id_empleada,:servicio,:fecha_servicio,:direccion,:ciudad,:horas,:hora_inicio,:hora_fin,:valor,:estado,:formapago,0, now())";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':consecutivo',$consecutivo);
      $stm->bindParam(':id_usuario',$id_usuario);
      $stm->bindParam(':id_empleada',$id_empleada);
      $stm->bindParam(':servicio',$servicio);
      $stm->bindParam(':fecha_servicio',$fecha);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':horas',$horas);
      $stm->bindParam(':hora_inicio',$hora_inicio);
      $stm->bindParam(':hora_fin',$hora_fin);
      $stm->bindParam(':valor',$valor);
      $stm->bindParam(':estado',$estado);
      $stm->bindParam(':formapago',$formapago);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function Actualizar_Usuario($user){
    $sql = "UPDATE usuarios SET nombres = :nombres, apellidos = :apellidos, correo = :correo, direccion = :direccion, ciudad = :ciudad,
    telefono = :telefono, habilitado = :habilitado WHERE id = :id";
    try {
      extract($user);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':nombres',$nombres);
      $stm->bindParam(':apellidos',$apellidos);
      $stm->bindParam(':correo',$correo);
      $stm->bindParam(':direccion',$direccion);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':telefono',$telefono);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function Ingresar_Tarifa($tarifa){
    $sql = "INSERT INTO tarifas(codigo_servicio, codigo_ciudad, horas, valor, valor_especial, anio, habilitado)
            VALUES (:codigo_servicio, :codigo_ciudad, :horas, :valor, :valor_especial, :anio, 1)";
    try {
      extract($tarifa);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':codigo_servicio',$codigo_servicio);
      $stm->bindParam(':codigo_ciudad',$codigo_ciudad);
      $stm->bindParam(':horas',$horas);
      $stm->bindParam(':valor',$valor);
      $stm->bindParam(':valor_especial',$valor_especial);
      $stm->bindParam(':anio',$anio);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function Actualizar_Tarifa($tarifa){
    $sql = "UPDATE tarifas SET valor = :valor, valor_especial = :valor_especial, habilitado = :habilitado WHERE id = :id";
    try {
      extract($tarifa);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id',$id);
      $stm->bindParam(':valor',$valor);
      $stm->bindParam(':valor_especial',$valor_especial);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function Actualizar_Servicio($id, $data){
    $sql = "UPDATE servicios SET fecha_servicio = :fecha, id_empleada = :empleada, hora_inicio = :hora_inicio, hora_fin = :hora_fin
    WHERE id = :id";
    try {
      extract($data);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':fecha',$fecha);
      $stm->bindParam(':empleada',$empleada);
      $stm->bindParam(':hora_inicio',$hora_inicio);
      $stm->bindParam(':hora_fin',$hora_fin);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function Eliminar_Servicio($id){
    $sql = "UPDATE servicios SET anulado = 1 WHERE id = :id";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function GuardarPaquete($paquete){
    
    $sql = "INSERT INTO paquetes(nombre, fecha_inicio, fecha_fin, horas, dias ,valor_normal, valor_descuento, habilitado)
    VALUES (:nombre, :fecha_inicio, :fecha_fin, :horas, :dias , :valor_normal, :valor_descuento, :habilitado)";

    try {
      extract($paquete);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':nombre',$nombre);
      $stm->bindParam(':fecha_inicio',$fecha_inicio);
      $stm->bindParam(':fecha_fin',$fecha_fin);
      $stm->bindParam(':horas',$horas);
      $stm->bindParam(':dias',$dias);
      $stm->bindParam(':valor_normal',$valor_normal);
      $stm->bindParam(':valor_descuento',$valor_descuento);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->execute();
      return $this->db->lastInsertId();
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function EliminarCiudadesPaquete($id){
    $sql = "DELETE FROM paquetes_ciudades WHERE id_paquete = :id";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function GuardarCiudadPaquete($id_paquete,$codigo_ciudad){
    
    $sql = "INSERT INTO paquetes_ciudades(id_paquete, codigo_ciudad)
            VALUES(:id_paquete, :codigo_ciudad)";
    try {
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':id_paquete',$id_paquete);
      $stm->bindParam(':codigo_ciudad',$codigo_ciudad);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function ActualizarPaquete($id, $paquete){
    
    $sql = "UPDATE paquetes set nombre = :nombre, fecha_inicio = :fecha_inicio, fecha_fin = :fecha_fin, 
    horas = :horas ,dias = :dias,valor_normal = :valor_normal, valor_descuento = :valor_descuento, habilitado = :habilitado WHERE id = :id";

    try {
      extract($paquete);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':nombre',$nombre);
      $stm->bindParam(':fecha_inicio',$fecha_inicio);
      $stm->bindParam(':fecha_fin',$fecha_fin);
      $stm->bindParam(':horas',$horas);
      $stm->bindParam(':dias',$dias);
      $stm->bindParam(':valor_normal',$valor_normal);
      $stm->bindParam(':valor_descuento',$valor_descuento);
      $stm->bindParam(':habilitado',$habilitado);
      $stm->bindParam(':id',$id);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function ActualizarClave($data, $clave){
    
    $sql = "UPDATE usuarios SET clave = MD5(:clave) WHERE correo = :email";

    try {
      extract($data);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':email',$email);
      $stm->bindParam(':clave',$clave);
      $stm->execute();
      return "";
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }
}