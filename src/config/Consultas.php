<?php
class Consultas {
    private $db;

    public function __construct(){
      $conn = new Conexion();
      $this->db = $conn->conexion();
    }

    public function obtenerGrupos(){
        $sql = "select codigo,detalle,icon from mtgrupos_especialidades where habilitado = 1";
        try{
            $stm = $this->db->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            if ($data){
              return json_encode($data);
            }else {
              return json_encode(array());
            }
            $stm = null;
          }catch(PDOException $e){
            return json_encode(array());
          }
    }

    public function obtenerTipoDocumento(){
      $sql = "select Codigo,Detalle from  mttipodoc where Habilitado = 1";
      try{
          $stm = $this->db->prepare($sql);
          $stm->execute();
          $data = $stm->fetchAll();
          if ($data){
            return json_encode($data);
          }else {
            return json_encode(array());
          }
          $stm = null;
        }catch(PDOException $e){
          return json_encode(array());
        }
  }

  public function obtenerServiciosEspecialesTodos($id){
    $sql = "select e.codigo,e.nombre, (select count(*) from especialidades_proveedores p where p.id_especialidad = e.codigo) as Cantidad, e.icono from mtespecialidades e where e.codigo_grupo = :grupo and e.habilitado = 1";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':grupo' => $id));
        $data = $stm->fetchAll();
        if ($data){
          return json_encode($data);
        }else {
          return json_encode(array());
        }
        $stm = null;
      }catch(PDOException $e){
        return json_encode(array());
      }
}

    public function obtenerServiciosEspeciales($id){
        $sql = "select e.codigo,e.nombre, (select count(*) from especialidades_proveedores p where p.id_especialidad = e.codigo) as Cantidad,e.icono from mtespecialidades e where e.codigo_grupo = :grupo and e.habilitado = 1
                AND (select count(*) from especialidades_proveedores p where p.id_especialidad = e.codigo) > 0";
        try{
            $stm = $this->db->prepare($sql);
            $stm->execute(array(':grupo' => $id));
            $data = $stm->fetchAll();
            if ($data){
              return json_encode($data);
            }else {
              return json_encode(array());
            }
            $stm = null;
          }catch(PDOException $e){
            return json_encode(array());
          }
    }

    public function obtenerCiudades(){
      $sql = "select id,codigo,nombre from mtciudades where habilitado = 1";
      try{
          $stm = $this->db->prepare($sql);
          $stm->execute();
          $data = $stm->fetchAll();
          if ($data){
            return json_encode($data);
          }else {
            return json_encode(array());
          }
          $stm = null;
        }catch(PDOException $e){
          return json_encode(array());
        }
    }

  public function consultarExisteUsuario($correo){
    $sql = "select id from usuarios where correo = :correo";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':correo' => $correo));
        $data = $stm->fetchAll();
        if ($data){
          return true;
        }else {
          return false;
        }
        $stm = null;
      }catch(PDOException $e){
        return false;
      }
  }

  public function consultarExisteUsuarioxCedula($cedula){
    $sql = "select id from usuarios where cedula = :cedula";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':cedula' => $cedula));
        $data = $stm->fetchAll();
        if ($data){
          return true;
        }else {
          return false;
        }
        $stm = null;
      }catch(PDOException $e){
        return false;
      }
  }
public function consultarExisteProveedor($data){
  extract($data);
  $sql = "SELECT cedula,nombres FROM proveedores WHERE cedula = :cedula";
  try{
      $stm = $this->db->prepare($sql);
      $stm->execute(array(':cedula' => $cedula));
      $data = $stm->fetchAll();
      if ($data){
        return true;
      }else {
        return false;
      }
      $stm = null;
    }catch(PDOException $e){
      return false;
    }
}

public function consultarExisteProveedorxEmail($data){
  extract($data);
  $sql = "SELECT cedula,nombres FROM proveedores WHERE email = :email";
  try{
      $stm = $this->db->prepare($sql);
      $stm->execute(array(':email' => $email));
      $data = $stm->fetchAll();
      if ($data){
        return true;
      }else {
        return false;
      }
      $stm = null;
    }catch(PDOException $e){
      return false;
    }
}

public function obtenerServicios(){
  $sql = "select codigo,descripcion,icon, habilitado from mtservicios where habilitado = 1";
  try{
      $stm = $this->db->prepare($sql);
      $stm->execute();
      $data = $stm->fetchAll();
      if ($data){
        return json_encode($data);
      }else {
        return json_encode(array());
      }
      $stm = null;
    }catch(PDOException $e){
      return json_encode(array());
    }
}

public function consultarValorServicio($data){
  extract($data);
  $sql = "call sp_ValorServicio('$servicio','$ciudad',$dias,$horas,'$fecha_inicio')";
  try{
      $stm = $this->db->prepare($sql);
      /*$stm->bindParam(':servicio',$servicio);
      $stm->bindParam(':ciudad',$ciudad);
      $stm->bindParam(':dias',$dias);
      $stm->bindParam(':hora',$horas);
      $stm->bindParam(':especial',$especial);*/
      $stm->execute();
      $data = $stm->fetchAll();
      if ($data){
        return json_encode($data);
      }else {
        return json_encode(array());
      }
      $stm = null;
    }catch(PDOException $e){
      return json_encode(array());
    }
}

public function obtenerUsuariosAsociados($id_usuario){
  $sql = "select u.id,u.nombre,u.direccion,u.ciudad,c.nombre as nombreCiudad,u.telefono,u.usuario_principal
          from usuarios_asociados u 
          inner join mtciudades c on c.codigo = u.ciudad
          where u.habilitado = 1 and u.id_usuario =:id_usuario";
  try{
      $stm = $this->db->prepare($sql);
      $stm->execute(array(':id_usuario' => $id_usuario));
      $data = $stm->fetchAll();
      if ($data){
        return json_encode($data);
      }else {
        return json_encode(array());
      }
      $stm = null;
    }catch(PDOException $e){
      return json_encode(array());
    }
}

public function consultarLogin($user){
  $sql = "select id,cedula,ciudad,nombres,apellidos, correo, ciudad,telefono,direccion from usuarios where correo = :correo and clave = :clave and habilitado = 1";
  try{
      extract($user);
      $clave = md5($clave);
      $stm = $this->db->prepare($sql);
      $stm->bindParam(':correo',$correo);
      $stm->bindParam(':clave',$clave);
      $stm->execute();
      $data = $stm->fetchAll();
      if ($data){
        return json_encode($data);
      }else {
        return json_encode(array());
      }
      $stm = null;
    }catch(PDOException $e){
      return json_encode(array());
    }
}

public function obtenerProveedores($especialidad){
  $sql = "SELECT p.nombres,p.apellidos,p.direccion, p.celular,p.sitio_web,p.descripcion,p.ciudad as codigoCiudad, c.nombre as ciudad, e.nombre AS especialidad
          FROM proveedores p 
          INNER JOIN especialidades_proveedores es ON es.id_proveedor = p.id
          INNER JOIN mtciudades c ON c.codigo = p.ciudad
          INNER JOIN mtespecialidades e ON e.codigo = es.id_especialidad
          WHERE es.id_especialidad = :especialidad AND p.habilitado = 0";
  try{
      $stm = $this->db->prepare($sql);
      $stm->execute(array(':especialidad' => $especialidad));
      $data = $stm->fetchAll();
      if ($data){
        return json_encode($data);
      }else {
        return json_encode(array());
      }
      $stm = null;
    }catch(PDOException $e){
      return json_encode(array());
    }
}

public function consultarConsecutivoServicio(){
  $sql = "select (IFNULL(MAX(Consecutivo),0) + 1) AS Consecutivo FROM servicios";
  try{
      $stm = $this->db->prepare($sql);
      $stm->execute();
      $data = $stm->fetchAll();
      if ($data){
        return intval($data[0]["Consecutivo"]);
      }else {
        return 0;
      }
      $stm = null;
    }catch(PDOException $e){
      return 0;
    }
}

public function obtenerDiasFestivos(){
  $sql = "SELECT fecha FROM dias_festivos";
  try{
      $stm = $this->db->prepare($sql);
      $stm->execute();
      $data = $stm->fetchAll();
      if ($data){
        return json_encode($data);
      }else {
        return json_encode(array());
      }
      $stm = null;
    }catch(PDOException $e){
      return json_encode(array());
    }
}

public function existeDiaFestivo($fecha){
  $sql = "SELECT fecha FROM dias_festivos where fecha=:fecha";
  try{
      $stm = $this->db->prepare($sql);
      $stm->execute(array(':fecha' => $fecha));
      $data = $stm->fetchAll();
      if ($data){
        return true;
      }else {
        return false;
      }
      $stm = null;
    }catch(PDOException $e){
      return false;
    }
}

public function consultarEmpleadaNuevoServicio($ciudad){
  $sql = "select codigo from empleadas 
          where ciudad = :ciudad order by cantidad_servicios asc limit 1";
  try{
      $stm = $this->db->prepare($sql);
      $stm->execute(array(':ciudad' => $ciudad));
      $data = $stm->fetchAll();
      if ($data){
        return intval($data[0]["codigo"]);
      }else {
        return 0;
      }
      $stm = null;
    }catch(PDOException $e){
      return 0;
    }
}

//////////////////////DASHBOARD//////////////////////////

  public function obtenerServiciosTodos(){
    $sql = "select codigo,descripcion,icon, habilitado from mtservicios";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function consultarLoginDashboard($user){
    extract($user);
    $sql = "SELECT id,usuario, nombre, email, perfil FROM usuarios_dashboard WHERE usuario = :usuario AND clave = :clave AND habilitado = 1";
    try{
      $stm = $this->db->prepare($sql);
      $stm->execute(array(':usuario' => $usuario,':clave' => $clave));
      $stm->execute();
      $data = $stm->fetchAll(PDO::FETCH_OBJ);
      if ($data){
        return $data;
      }else {
        return array();
      }
      $stm = null;
    }catch(PDOException $e){
      return array();
    }
  }

  public function obtenerEmpleadas(){
    $sql = "SELECT e.id, e.codigo, e.cedula, upper(e.nombre) as nombre, e.ciudad,c.nombre AS nombre_ciudad, e.direccion, e.celular,e.email,e.habilitado, e.imagen 
    FROM empleadas e
    INNER JOIN mtciudades c ON c.codigo = e.ciudad";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function consultarConsecutivoEmpleada(){
    $sql = "SELECT MAX(IFNULL(codigo,0)) + 1 AS consecutivo FROM empleadas";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll();
        if ($data){
          return intval($data[0]["consecutivo"]);
        }else {
          return 0;
        }
        $stm = null;
      }catch(PDOException $e){
        return 0;
      }
  }

  public function obtenerMtCiudades(){
    $sql = "select id,codigo,nombre, habilitado from mtciudades where habilitado = 1";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarCiudades(){
    $sql = "select id,codigo,nombre, habilitado from mtciudades";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarGrupoEspecialidades(){
    $sql = "SELECT codigo, detalle, icon FROM mtgrupos_especialidades";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarEspecialidadesxGrupo($codigo_grupo){
    $sql = "SELECT codigo, nombre, icono, habilitado FROM mtespecialidades WHERE codigo_grupo = :codigo_grupo";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':codigo_grupo' => $codigo_grupo));
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarProveedores($id){
    $sql = "SELECT p.id,p.tipodoc, p.cedula, p.nombres, p.apellidos, p.direccion, p.ciudad,ciu.nombre AS nombre_ciudad, p.celular, p.email,
    p.sitio_web, p.descripcion, p.nombre_referencia_personal, p.telefono_referencia_personal,
    p.nombre_referencia_familiar, p.telefono_referencia_familiar, p.habilitado
    FROM proveedores p
    INNER JOIN especialidades_proveedores e ON e.id_proveedor = p.id
    INNER JOIN mtciudades ciu ON ciu.codigo = p.ciudad
    WHERE e.id_especialidad = :id";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':id' => $id));
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarEspecialidadexProveedor($id){
    $sql = "SELECT e.id_especialidad, g.codigo_grupo, g.nombre FROM especialidades_proveedores e
    INNER JOIN mtespecialidades g ON g.codigo = e.id_especialidad
    WHERE e.id_proveedor = :id";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':id' => $id));
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarClientes(){
    $sql = "SELECT id, cedula, upper(rtrim(concat(nombres , ' ' , apellidos))) AS nombre FROM usuarios where habilitado = 1";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarClientesAsociados($id){
    $sql = "SELECT u.id, rtrim(upper(u.nombre)) as nombre, u.direccion, u.ciudad, u.telefono, u.usuario_principal, c.nombre as nombreCiudad, u.habilitado,u.usuario_principal
    FROM usuarios_asociados u 
    INNER JOIN mtciudades c on c.codigo = u.ciudad
    WHERE u.id_usuario = :id";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':id' => $id));
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarFormasPago(){
    $sql = "SELECT codigo, detalle FROM mtformaspago WHERE habilitado = 1";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarAgenda(){
    $sql = "SELECT s.id,u.nombre, m.descripcion AS servicio, s.fecha_servicio,s.hora_inicio,s.hora_fin FROM servicios s
    INNER JOIN usuarios_asociados u ON u.id = s.id_usuario
    INNER JOIN mtservicios m ON m.codigo = s.servicio
    WHERE s.fecha_servicio >= date(NOW()) and s.anulado = 0";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarUsuarios(){
    $sql = "SELECT u.id, u.cedula, u.nombres, u.apellidos, CONCAT(u.nombres, ' ', u.apellidos) AS nombre,
    u.correo, u.direccion, u.ciudad,c.nombre AS nombre_ciudad, u.telefono, u.habilitado,
    (SELECT COUNT(*) FROM usuarios_asociados a WHERE a.id_usuario = u.id AND a.usuario_principal = 0) as cantidadUsuarios 
    FROM usuarios u
    INNER JOIN mtciudades c ON c.codigo = u.ciudad";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarTarifas(){
    $sql = "SELECT t.id,t.codigo_servicio, t.codigo_ciudad,c.nombre AS ciudad,s.descripcion AS servicio, t.anio, t.habilitado 
    FROM tarifas t 
    INNER JOIN mtciudades c ON c.codigo = t.codigo_ciudad
    INNER JOIN mtservicios s ON s.codigo = t.codigo_servicio
    GROUP BY t.codigo_servicio, t.codigo_ciudad, t.anio
    ORDER BY t.anio desc";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarTarifa($datos){
    extract($datos);
    $sql = "SELECT t.id,t.horas, t.valor, t.valor_especial, t.habilitado FROM tarifas t 
    WHERE t.codigo_servicio = :codigo_servicio and t.codigo_ciudad = :codigo_ciudad and t.anio = :anio";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':codigo_servicio' => $codigo_servicio,':codigo_ciudad' => $codigo_ciudad,':anio' => $anio));
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarExisteTarifa($tarifa){
    $sql = "SELECT * FROM tarifas WHERE codigo_servicio = :codigo_servicio AND codigo_ciudad = :codigo_ciudad AND anio = :anio";
    try{
        extract($tarifa);
        $stm = $this->db->prepare($sql);
        $stm->bindParam(':codigo_servicio',$codigo_servicio);
        $stm->bindParam(':codigo_ciudad',$codigo_ciudad);
        $stm->bindParam(':anio',$anio);
        $stm->execute();
        $data = $stm->fetchAll();
        if ($data){
          return true;
        }else {
          return false;
        }
        $stm = null;
      }catch(PDOException $e){
        return false;
      }
  }

  public function ConsultarInfoServicio($id){
    $sql = "SELECT s.id_empleada, s.fecha_servicio, s.ciudad , s.horas, s.hora_inicio,
            s.servicio, s.valor, u.nombre, s.direccion, date(s.fecha_ing) as fecha_ing
            FROM servicios s 
            INNER JOIN usuarios_asociados u on u.id = s.id_usuario
            WHERE s.id = :id";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':id' => $id));
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarDatosGraficas($tipo){
    $sql = "CALL sp_Graficas($tipo)";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarReportes($tipo){
    $sql = "CALL sp_Reportes($tipo)";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function ConsultarPaquetes(){
    $sql = "SELECT id, nombre, fecha_inicio, fecha_fin, horas, dias, valor_normal, valor_descuento, habilitado 
    FROM paquetes order by id desc";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  
  public function ConsultarCiudadesPaquete($id){
    $sql = "SELECT codigo_ciudad FROM paquetes_ciudades WHERE id_paquete = :id";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':id' => $id));
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        if ($data){
          return $data;
        }else {
          return array();
        }
        $stm = null;
      }catch(PDOException $e){
        return array();
      }
  }

  public function consultarExisteUsuarioxEmail($data){
    extract($data);
    $sql = "SELECT * FROM usuarios WHERE correo = :email";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':email' => $email));
        $data = $stm->fetchAll();
        if ($data){
          return true;
        }else {
          return false;
        }
        $stm = null;
      }catch(PDOException $e){
        return false;
      }
  }

  public function ConsultarServiciosxUsuario($id){
    $sql = "SELECT s.id, s.horas,s.direccion,s.fecha_servicio,s.hora_inicio,s.valor, trim(e.detalle) AS estado,
    TRIM(c.nombre) AS ciudad
    FROM servicios s
    INNER JOIN mtciudades c ON c.codigo = s.ciudad
    INNER JOIN mtestservicio e ON e.id = s.estado
    WHERE s.id_usuario = :id
    order by s.fecha_servicio desc
    ";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':id' => $id));
        $data = $stm->fetchAll();
        if ($data){
          return json_encode($data);
        }else {
          return json_encode(array());
        }
        $stm = null;
      }catch(PDOException $e){
        return json_encode(array());
      }
  }

  public function ConsultarUsuario($id){
    $sql = "SELECT u.id, u.cedula, u.nombres, u.apellidos, CONCAT(u.nombres, ' ', u.apellidos) AS nombre,
    u.correo, u.direccion, u.ciudad,c.nombre AS nombre_ciudad, u.telefono, u.habilitado,
    (SELECT COUNT(*) FROM usuarios_asociados a WHERE a.id_usuario = u.id AND a.usuario_principal = 0) as cantidadUsuarios 
    FROM usuarios u
    INNER JOIN mtciudades c ON c.codigo = u.ciudad
    where u.id = :id
    ";
    try{
        $stm = $this->db->prepare($sql);
        $stm->execute(array(':id' => $id));
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_OBJ);
        $stm = null;
        if ($data){
          return $data;
        }else {
          return array();
        }
      }catch(PDOException $e){
        return array();
      }
  }
}



